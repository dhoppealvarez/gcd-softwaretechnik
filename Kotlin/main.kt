import kotlin.math.abs

fun main() {
    println(gcd(12, 18))
    println(gcd(16, 20))
    println(gcd(120, 900))
    println(gcd(105, 26))
    }

fun gcd(a: Int, b: Int): Int {
    if (a == 0) {
        return abs(b)
    }
    if (b == 0) {
        return abs(a)
    }
    var h: Int
    var x = a
    var y = b
    while (y != 0) {
        h = x.rem(y)
        x = y
        y = h
    }
    return abs(x)
}

