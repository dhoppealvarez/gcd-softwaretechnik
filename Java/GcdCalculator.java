public class GcdCalculator {
    public static void main (String[] args) {
        System.out.println("gcd(12, 18) = " + gcd(12, 18));
        System.out.println("gcd(16, 20) = " + gcd(16, 20));
        System.out.println("gcd(120, 900) = " + gcd(120, 900));
        System.out.println("gcd(105, 26) = " + gcd(105, 26));
     }

    public static int gcd(int a, int b) {
        if (a == 0) {
            return Math.abs(b);
        }

        if (b == 0) {
            return Math.abs(a);
        }

        while (b != 0) {
            int h = a % b;
            a = b;
            b = h;
        }

        return Math.abs(a);
    }
}
