package main

import (
	"fmt"
)

func main() {
	fmt.Println("GCD 12, 18", gcd(12, 18))
	fmt.Println("GCD 16, 20", gcd(16, 20))
	fmt.Println("GCD 120, 900", gcd(120, 900))
	fmt.Println("GCD 105, 26", gcd(105, 26))
}

func gcd(a, b int) int {

	if a == 0 {
		return Abs(b)
	}

	if b == 0 {
		return Abs(a)
	}

	// while does not exist in go
	for b != 0 {
		h := a % b
		a = b
		b = h
	}

	return Abs(a)
}

// Abs calc absolut of an int, Go has no built-in library function for computing the absolute value of an integer
func Abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}
