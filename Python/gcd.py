def gcd(a,b): 

    if (a == 0): 
        return abs(b)

    if (b == 0): 
        return abs(a)

    while b != 0:
        h = a % b
        a = b
        b = h
    
    return abs(a)
    

print(gcd(12,18))
print(gcd(16, 20))
print(gcd(120, 900))
print(gcd(105, 26))