#include <stdio.h>
#include <stdlib.h>

int gcd(int a, int b) {

    if(a == 0 )
        return abs(b); 
    
    if(b == 0)
        return abs(a); 
    
    while (b != 0)
    {
        /* code */
        const int h =  a % b;
        a = b;
        b = h; 
    }

    return abs(a);
    
}

int main() {
    printf("GCD ( %d , %d ) is %d.\n", 12, 18, gcd(12, 18));
    printf("GCD ( %d , %d ) is %d.\n", 16, 20, gcd(16, 20));
    printf("GCD ( %d , %d ) is %d.\n", 120, 900, gcd(120, 900));
    printf("GCD ( %d , %d ) is %d.\n", 105, 26, gcd(105, 26));

    return 0;
}
